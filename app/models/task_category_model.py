from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from dataclasses import dataclass

@dataclass
class Task_category(db.Model):

    id: int
    task_id: int
    category_id: int

    __tablename__ = 'tasks_categories'

    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer, db.ForeignKey('tasks.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))

    task = relationship("Task", backref=backref("task_category"))
    category = relationship("Category", backref=backref("task_category"))