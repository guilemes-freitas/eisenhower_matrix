from flask import Flask

from .category_views import bp as bp_category
from .task_views import bp as bp_task
from .all_info_views import bp as bp_all_info

def init_app(app: Flask):
    app.register_blueprint(bp_category)
    app.register_blueprint(bp_task)
    app.register_blueprint(bp_all_info)