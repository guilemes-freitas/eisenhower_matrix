from flask import Blueprint, request, current_app, jsonify
from app.models.category_model import Category
import sqlalchemy

bp = Blueprint("category", __name__)

@bp.post("/category")
def create_category():
    data = request.get_json()
    try:
        new_category = Category(
            name = data["name"],
            description = data["description"]
        )
        try:
            session = current_app.db.session

            session.add(new_category)
            session.commit()

            return jsonify(new_category),201
        except sqlalchemy.exc.IntegrityError:
            return {"Message": "Category already exists!"}, 409
    except KeyError as e:
        return {"KeyError": str(e)},400

@bp.patch("/category/<int:id>")
def update_category(id: int):
    data = request.get_json()
    try:
        query = Category.query.get(id)
        for key, value in data.items():
            setattr(query, key, value)
        session = current_app.db.session

        session.add(query)
        session.commit()
        query = Category.query.get(id)

        return jsonify(query), 200
    except AttributeError:
        return {"Message": "Category not found!"}, 404

@bp.delete("/category/<int:id>")
def delete_category(id: int):
    data = request.get_json()
    try:
        query = Category.query.get(id)
        session = current_app.db.session

        session.delete(query)
        session.commit()
        return '', 204
    except (AttributeError,sqlalchemy.orm.exc.UnmappedInstanceError):
        return {"Message": "Category not found!"}, 404