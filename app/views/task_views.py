from dataclasses import asdict
from app.models.category_model import Category
from app.models.task_category_model import Task_category
from app.models.eisenhower_model import Eisenhower
from flask import Blueprint, request, current_app, jsonify
from app.models.task_model import Task
import sqlalchemy

bp = Blueprint("task", __name__)

@bp.post("/task")
def create_task():
    data = request.get_json()
    eisenhower_options = [[1,2],[3,4]]
    try:
        session = current_app.db.session
        category_ids = []
        categories = []
        for category in data["category"]:
            category_exists = Category.query.filter_by(name=category["name"]).first()
            try:
                category_ids.append(category_exists.id)
                categories.append(category_exists.name)
            except AttributeError:
                new_category = Category(
                    name = category["name"],
                    description = ""
                )
                session.add(new_category)
                session.commit()
                category_ids.append(new_category.id)
                categories.append(new_category.name)

        if (data["importance"] == 1 or data["importance"] == 2) and (data["urgency"] == 1 or data["urgency"] == 2):
            eisenhower_id = eisenhower_options[data["importance"]-1][data["urgency"]-1]
            new_task = Task(
                name = data["name"],
                description = data["description"],
                duration = data["duration"],
                importance = data["importance"],
                urgency = data["urgency"],
                eisenhower_id = eisenhower_id
            )
            session.add(new_task)
            session.commit()
            eisenhower = asdict(Eisenhower.query.get(eisenhower_id))

            session = current_app.db.session

            for category_id in category_ids:
                new_task_categories = Task_category(
                    task_id = new_task.id,
                    category_id = category_id
                )
                session.add(new_task_categories)
            session.commit()

            return {"id":new_task.id,
                    "name":new_task.name,
                    "description":new_task.description,
                    "duration":new_task.duration,
                    "eisenhower_classification": eisenhower["type"],
                    "category":[{"name":category} for category in categories]},201
        else:
            return{"error": {
                "valid_options": {
                    "importance": [
                        1,
                        2
                    ],
                    "urgency": [
                        1,
                        2
                    ]
                },
                "received_options":{
                    "importance": data["importance"],
                    "urgency": data["urgency"]
                }
            }}, 404
    except KeyError as e:
        return {"KeyError": str(e)},400
    except sqlalchemy.exc.IntegrityError:
                return {"Message": "Task already exists!"}, 409

@bp.patch("/task/<int:id>")
def update_task(id: int):
    eisenhower_options = [[1,2],[3,4]]
    data = request.get_json()
    try:
        query = Task.query.get(id)
        for key, value in data.items():
            setattr(query, key, value)

        session = current_app.db.session

        session.add(query)
        session.commit()
        query = Task.query.get(id)
        setattr(query, 'eisenhower_id', eisenhower_options[query.importance -1][query.urgency -1])
        session.add(query)
        session.commit()

        query = Task.query.get(id)
        eisenhower = Eisenhower.query.get(query.eisenhower_id)
        return {"id":query.id,
                    "name":query.name,
                    "description":query.description,
                    "duration":query.duration,
                    "eisenhower_classification": eisenhower.type},200
    except AttributeError:
        return {"Message": "task not found!"}, 404

@bp.delete("/task/<int:id>")
def delete_task(id: int):
    try:
        removed_task_category = Task_category.query.filter_by(task_id=id).first()
        removed_task = Task.query.get(id)
        session = current_app.db.session

        session.delete(removed_task_category)
        session.delete(removed_task)
        session.commit()
        return '', 204
    except (AttributeError,sqlalchemy.orm.exc.UnmappedInstanceError):
        return {"Message": "task not found!"}, 404