from app.models.eisenhower_model import Eisenhower
from app.models.task_category_model import Task_category
from app.models.task_model import Task
from flask import Blueprint, request, current_app, jsonify
from app.models.category_model import Category

bp = Blueprint("all_info", __name__)

@bp.get("/")
def get_all():
    query = Category.query.all()
    response = []
    for category in query:
        task_categories = Task_category.query.filter_by(category_id = category.id)
        tasks_ids = [task_id.task_id for task_id in task_categories]
        tasks = []
        for task_id in tasks_ids:
            task = Task.query.get(task_id)
            eisenhower = Eisenhower.query.get(task.eisenhower_id)

            tasks.append({"id":task.id,
                        "name":task.name,
                        "description":task.description,
                        "priority": eisenhower.type}) 

        response.append({"id": category.id, 
            "name": category.name, 
            "description": category.description,
            "tasks": [task for task in tasks]})
    return jsonify(response),200